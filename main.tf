resource "aws_subnet" "subnet" {
  for_each          = { for k, v in var.subnets: k => v}
  vpc_id            = var.vpc_id
  cidr_block        = each.value.cidr
  availability_zone = "${var.region}${each.value.zone}"

  tags = {
    Name        = "${var.project}-${var.env}-main-app-private-1${each.value.zone}"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = "network"
    SubnetType  = "private"
  }
}

resource "aws_route_table" "rt_private" {
  vpc_id           = var.vpc_id

  route {
    cidr_block     = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.ngw.id
  }

  tags = {
    Name        = "${var.project}-${var.env}-main-rt-private"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = "network"
  }
}

resource "aws_route_table_association" "rta" {
  for_each       = aws_subnet.subnet
  subnet_id      = each.value.id
  route_table_id = aws_route_table.rt_private.id
}

#############################
# NAT Gateway
#############################
resource "aws_eip" "ngw_eip" {
  vpc = true

  tags = {
    Name        = "${var.project}-${var.env}-main-ngw-eip"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = "network"
  }
}

resource "aws_nat_gateway" "ngw" {
  allocation_id = aws_eip.ngw_eip.id
  subnet_id     = var.public_subnet_id

  tags = {
    Name        = "${var.project}-${var.env}-main-ngw"
    project     = var.project
    environment = var.env
    terraform   = true
    component   = "network"
  }
}

