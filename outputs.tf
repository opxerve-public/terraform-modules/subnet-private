output "ngw" {
  value = aws_eip.ngw_eip.public_ip
}
output "rt_private_id" {
  value = aws_route_table.rt_private.id
}
output "subnet" {
  value = aws_subnet.subnet
}
output "subnet_ids" {
  value = [for sbn in aws_subnet.subnet: sbn.id]
}
