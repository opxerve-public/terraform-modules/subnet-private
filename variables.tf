variable "project" {}
variable "region" {}
variable "env" {}

#############################
# Main VPC
#############################
variable "vpc_id" {}
variable "subnets" {}
variable "public_subnet_id" {}
